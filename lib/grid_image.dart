import 'dart:math';

import 'package:flutter/material.dart';

class GridImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset('images/grid_image.jpg'),
        GridOverlay(150, 75)
      ],
    );
  }
}

class GridOverlay extends StatelessWidget {
  final int _rows, _columns;

  GridOverlay(this._rows, this._columns);

  @override
  Widget build(BuildContext context) {
    List<Flexible> gridColumns = List.generate(_columns, (int index) => gridColumn);

    return Container(
      child: Row(
        children: gridColumns,
      ),
    );
  }

  Flexible get gridColumn {
    List<Flexible> gridItems = List<Flexible>.generate(_rows, (int index) => gridItem);

    return Flexible(
      child: Column(
        children: gridItems,
      ),
    );
  }

  Flexible get gridItem {
    Random random = Random();

    double shouldBeTransparent = 0.50;
    double randomDouble = random.nextDouble();
    Color fullColor = Colors.teal;
    if(randomDouble < shouldBeTransparent) {
      fullColor = fullColor.withAlpha(random.nextInt(255));
    }

    return Flexible(
      child: Container(
        color: fullColor,
      ),
    );
  }
}
